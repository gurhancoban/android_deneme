package com.example.omer.webviewkullanimi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {
    WebView webV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webV=(WebView)findViewById(R.id.webView);

        webV.getSettings().setJavaScriptEnabled(true);

        webV.setWebViewClient(new WebViewClient());
        webV.setWebChromeClient(new WebChromeClient());

        webV.loadUrl("http://gurhancoban.com");
    }
}
