﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class soru : MonoBehaviour {
    string url = "http://gurhancoban.com/android/bilgi/veri.php";
    // Use this for initialization
    public string _soru;
    public string sik_a;
    public string sik_b;
    public string sik_c;
    public string sik_d;
    public string cevap;
    public InputField a;
    public InputField b;
    public InputField c;
    public InputField d;
    public InputField sorum;
    public Dropdown cevap_;


    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void soru_ekle()
    {
        sik_a = a.text.ToString();
        sik_b = b.text.ToString();
        sik_c = c.text.ToString();
        sik_d = d.text.ToString();
        cevap = cevap_.value.ToString();
        _soru = sorum.text.ToString();
        WWWForm form = new WWWForm();
        form.AddField("durum", "giris");
        form.AddField("action", "gonder");
        form.AddField("soru", _soru);
        form.AddField("sik_a", sik_a);
        form.AddField("sik_b", sik_b);
        form.AddField("sik_c", sik_c);
        form.AddField("sik_d", sik_d);
        form.AddField("cevap", cevap);
        form.AddField("sorulma", 0);
        form.AddField("bilinme", 0);
        form.AddField("user", PlayerPrefs.GetString("email"));
        WWW www = new WWW(url, form);

         StartCoroutine(WaitForRequest(www));
         

        Debug.Log(cevap);
    }
    public void iptal()
    {
        a.text = "";
        b.text = "";
        c.text = "";
        d.text="";
        cevap_.value=0;
        sorum.text="";
    }
    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            a.text = "";
            b.text = "";
            c.text = "";
            d.text = "";
            cevap_.value = 0;
            sorum.text = "";
        }
        else
        {
            Debug.Log("WWW Eror:" + www.error);
        }
    }
}
