﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class soru_cek : MonoBehaviour {
    public string url1 = "http://gurhancoban.com/android/bilgi/soru_cek.php?durum=hazirlik";
    public string url2 = "http://gurhancoban.com/android/bilgi/veri.php";
    public string itemsDatastring;
    public string cevap;
    public int sorulma;
    public int bilinme;
    public int oran;
    public int id;
    public Button a;
    public Button b;
    public Button c;
    public Button d;
    public Text soru;
    public string durum = "bos";
    public string dogru;
    public float zaman = 30.0f;
    public Text text;
    public Text uyari;
    public bool zamanlayici=false;

    // Use this for initialization
    IEnumerator Start () {
        WWW itemsdata = new WWW(url1);
        yield return itemsdata;
        itemsDatastring = itemsdata.text;
        soru.text = GetDataValue(itemsDatastring, "soru:");
        a.GetComponentInChildren<Text>().text = GetDataValue(itemsDatastring, "a:");
        b.GetComponentInChildren<Text>().text = GetDataValue(itemsDatastring, "b:");
        c.GetComponentInChildren<Text>().text = GetDataValue(itemsDatastring, "c:");
        d.GetComponentInChildren<Text>().text = GetDataValue(itemsDatastring, "d:");
        cevap = GetDataValue(itemsDatastring, "cevap:");
        sorulma = int.Parse(GetDataValue(itemsDatastring, "sorulma:"));
        bilinme = int.Parse(GetDataValue(itemsDatastring, "bilinme:"));
        id = int.Parse(GetDataValue(itemsDatastring, "sn:"));
        zamanlayici = true;
    }
    public void Update()
    {
        if (zamanlayici == true)
        {
            zaman -= Time.deltaTime;
            text.text = zaman.ToString("0");
            if (zaman <= 0)
            {
                zamanlayici = false;
                uyari.text = "Süre Doldu";
                //LoadingEkrani.LevelYukle("anasayfa");
                yanlis_arttirma();

            }
        }

    }
   

    public static string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if(value.Contains("||")) value = value.Remove(value.IndexOf("||"));
        return value;
    }
    public void cevapla()
    {
        if (cevap == EventSystem.current.currentSelectedGameObject.name.ToUpper())
        {
            var colors = EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image;
            colors.color = new Color32(19, 98, 55, 255);
            EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image = colors;
            durum = "doğru";
            dogru_arttirma();
        }
        else
        {
            var colors = EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image;
            colors.color = new Color32(159, 28, 28, 255);
            EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image = colors;
            durum = "yanlış";
            yanlis_arttirma();
        }
    }
    public void dogru_arttirma()
    {
        a.enabled = false;
        b.enabled = false;
        c.enabled = false;
        d.enabled = false;
        kullanici.ds++;
        bilinme++;
        sorulma++;
        WWWForm form = new WWWForm();
        form.AddField("durum", "guncelle");
        form.AddField("id", id);
        form.AddField("sorulma", sorulma + 1);
        form.AddField("bilinme", bilinme + 1);
        form.AddField("oran", bilinme / sorulma);
        //LoadingEkrani.LevelYukle(SceneManager.GetActiveScene().name);
        WWW www = new WWW(url2, form);

        StartCoroutine(WaitForRequest(www, "dogru"));
    }
    public void yanlis_arttirma()
    {
        kullanici.ys++;
        a.enabled = false;
        b.enabled = false;
        c.enabled = false;
        d.enabled = false;
        sorulma++;
        //LoadingEkrani.LevelYukle("anasayfa");
        WWWForm form = new WWWForm();
        form.AddField("durum", "guncelle");
        form.AddField("id", id);
        form.AddField("sorulma", sorulma);
        form.AddField("bilinme", bilinme);
        form.AddField("oran", bilinme / sorulma);
        //LoadingEkrani.LevelYukle(SceneManager.GetActiveScene().name);
        WWW www = new WWW(url2, form);

        StartCoroutine(WaitForRequest(www, "yanlis"));
    }
    public void kullanici_deger_ekle()
    {
        WWWForm form = new WWWForm();
        form.AddField("durum", "kullanici_bilgi_h");
        form.AddField("dogru_sayisi",kullanici.ds);
        form.AddField("yanlis_sayisi", kullanici.ys);
        form.AddField("email", PlayerPrefs.GetString("email"));

        WWW www = new WWW(url2, form);
        StartCoroutine(kullanici_kaydi_bekle(www));
        
    }
    IEnumerator WaitForRequest(WWW www, string sonuc)
    {
        yield return www;
        if (www.error == null)
        {
            if (sonuc == "dogru")
            {
                LoadingEkrani.LevelYukle(SceneManager.GetActiveScene().name);
            }
            if (sonuc == "yanlis")
            {
                kullanici_deger_ekle();
                PlayerPrefs.SetInt("ds", kullanici.ds);
                PlayerPrefs.SetInt("ys", kullanici.ys);
                LoadingEkrani.LevelYukle("anasayfa");
            }

        }
        else
        {
            Debug.Log("WWW Eror:" + www.error);
        }
    }
    IEnumerator kullanici_kaydi_bekle(WWW www)
    {
        yield return www;
    }

}
