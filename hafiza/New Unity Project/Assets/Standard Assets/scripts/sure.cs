﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class sure : MonoBehaviour {
    Text geriSayimYazi;

    float geriSayim = 10.0F;
    // Use this for initialization
    void Start () {
        geriSayimYazi = GameObject.Find("saniye").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        geriSayim -= Time.deltaTime;
        if(geriSayim>0)
        {
            geriSayimYazi.text = geriSayim.ToString("#.0");
        }
	}
}
