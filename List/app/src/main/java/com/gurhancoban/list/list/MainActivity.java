package com.gurhancoban.list.list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        if(Fonksiyonlar.giriskontrol(getApplicationContext())){
            Intent kayit=new Intent(MainActivity.this,MainScreen.class);
            MainActivity.this.startActivity(kayit);
            MainActivity.this.finish();
        }else{
            Intent giris=new Intent(MainActivity.this,Login.class);
            MainActivity.this.startActivity(giris);
            MainActivity.this.finish();
        }
    }
}
