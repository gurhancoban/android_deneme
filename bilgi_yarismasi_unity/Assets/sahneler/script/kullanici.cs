﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class kullanici : MonoBehaviour {
    string url = "http://gurhancoban.com/android/bilgi/kullanici.php";
    //login
    public InputField kullanici_adi_g;
    public InputField sifre_g;
    public static string   isim;
    public static int ds;
    public static int ys;
    public static int kazanilan;
    public static int kaybedilen;

    public InputField kullanici_adi_t;
    public InputField kullanici_adi_k;
    public InputField sifre_k;
    public Text uyariY;

    public string itemsDatastring;
    public string email;
    void Start()
    {
        if (PlayerPrefs.GetInt("login") == 1)
        {
            isim = PlayerPrefs.GetString("isim");
            ds = PlayerPrefs.GetInt("ds");
            ys = PlayerPrefs.GetInt("ys");
            kazanilan = PlayerPrefs.GetInt("kazanilan");
            kaybedilen = PlayerPrefs.GetInt("kaybedilen");
            tarih_guncelle();
            LoadingEkrani.LevelYukle("anasayfa");
            
        }
    }
    public void kayit()
    {
        if (kullanici_adi_k.text == kullanici_adi_t.text)
        {
            uyariY.enabled = false;
            WWWForm form = new WWWForm();
            form.AddField("action", "kayit");
            form.AddField("email", kullanici_adi_k.text);
            form.AddField("password", sifre_k.text);
            WWW www = new WWW(url, form);

            StartCoroutine(WaitForRequest(www, "kayit"));
        }
        else
        {
            uyariY.enabled = true;
            uyariY.text = "Mail adresleri aynı değil";
        }

    }

    public void giris()
    {
        uyariY.enabled = false;
        WWWForm form = new WWWForm();
        form.AddField("action", "giris");
        form.AddField("email", kullanici_adi_g.text);
        form.AddField("password", sifre_g.text);
        WWW www = new WWW(url, form);

        StartCoroutine(WaitForRequest(www, "giris"));
        
    }

    IEnumerator WaitForRequest(WWW www, string islem)
    {
        if(islem=="kayit")
        { 
            yield return www;
            if (www.error == null)
            {
                itemsDatastring = www.text;
                Debug.Log(itemsDatastring);
                if (itemsDatastring.ToString() == "hata")
                {
                    uyariY.text = "Bu Mail adresi ile daha önce kayıt olunmuştur.";
                    uyariY.enabled = true;
                }
                else
                {
                    uyariY.text = "kaydınız oluşturulmuştur.";
                    uyariY.enabled = true;
                }

            }
            else
            {
                Debug.Log("WWW Eror:" + www.error);
            }

        }
        else if(islem=="giris")
        {
            yield return www;
            if (www.error == null)
            {
                itemsDatastring = www.text;
                if (itemsDatastring.ToString() == "sifre_bos")
                {
                    uyariY.text = "Kullanıcı adı veya şifre alanı boş bırakılamaz.";
                    uyariY.enabled = true;
                }
                else if(itemsDatastring.ToString() == "sifre_hata")
                {
                    uyariY.text = "Kullanici adi veya şifreniz hatalıdır.";
                    uyariY.enabled = true;
                }
                else
                {
                    uyariY.enabled = false;
                    if(GetDataValue(itemsDatastring, "isim:") == "")
                    {
                        isim = GetDataValue(itemsDatastring, "email:");
                    }
                    else
                    {
                        isim = GetDataValue(itemsDatastring, "isim:");
                    }
                    ds =int.Parse(GetDataValue(itemsDatastring, "dogru_sayisi:"));
                    ys = int.Parse(GetDataValue(itemsDatastring, "yanlis_sayisi:"));
                    kazanilan = int.Parse(GetDataValue(itemsDatastring, "kazanilan_oyun:"));
                    kaybedilen = int.Parse(GetDataValue(itemsDatastring, "kaybedilen_oyun:"));
                    PlayerPrefs.SetString("email", GetDataValue(itemsDatastring, "email:"));
                    PlayerPrefs.SetInt("login", 1);
                    PlayerPrefs.SetString("isim", isim);
                    PlayerPrefs.SetInt("ds", ds);
                    PlayerPrefs.SetInt("ys", ys);
                    PlayerPrefs.SetInt("kazanilan", kazanilan);
                    PlayerPrefs.SetInt("kaybedilen", kaybedilen);
                    Debug.Log(PlayerPrefs.GetInt("login"));
                    tarih_guncelle();
                    LoadingEkrani.LevelYukle("anasayfa");
                }
            }
            else
            {
                Debug.Log("WWW Eror:" + www.error);
            }
        }
        else if (islem == "diger")
        {
            Debug.Log("ok");
        }

    }
    string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("||")) value = value.Remove(value.IndexOf("||"));
        return value;
    }

    public void tarih_guncelle()
    {
        uyariY.enabled = false;
        WWWForm form = new WWWForm();
        form.AddField("action", "guncelle");
        form.AddField("email", PlayerPrefs.GetString("email"));
        WWW www = new WWW(url, form);
        Debug.Log(PlayerPrefs.GetString("email"));
        StartCoroutine(WaitForRequest(www, "diger"));
    }
}
