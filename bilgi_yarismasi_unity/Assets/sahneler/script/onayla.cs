﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class onayla : MonoBehaviour {
    public string url1 = "http://gurhancoban.com/android/bilgi/veri.php";
    public string url2 = "http://gurhancoban.com/android/bilgi/soru_cek.php?durum=onay";
    public string itemsDatastring;
    public Text soru;
    public Dropdown zorluk;
    public Dropdown dil;
    // Use this for initialization
    IEnumerator Start()
    {
        WWW itemsdata = new WWW(url2);
        yield return itemsdata;
        itemsDatastring = itemsdata.text;
        soru.text = soru_cek.GetDataValue(itemsDatastring, "soru:")+"\n------------------\n"+"A) "+ soru_cek.GetDataValue(itemsDatastring, "a:") + "\n" + "B) " + soru_cek.GetDataValue(itemsDatastring, "b:") + "\n" + "C) " + soru_cek.GetDataValue(itemsDatastring, "c:") + "\n" + "D) " + soru_cek.GetDataValue(itemsDatastring, "d:") + "\n" + "Cevap:  " + soru_cek.GetDataValue(itemsDatastring, "cevap:");
        
    }
    
    public void onay()
    {
        WWWForm form = new WWWForm();
        form.AddField("durum", "onay");
        form.AddField("id", soru_cek.GetDataValue(itemsDatastring, "sn:"));
        form.AddField("soru", soru_cek.GetDataValue(itemsDatastring, "soru:"));
        form.AddField("sik_a", soru_cek.GetDataValue(itemsDatastring, "a:"));
        form.AddField("sik_b", soru_cek.GetDataValue(itemsDatastring, "b:"));
        form.AddField("sik_c", soru_cek.GetDataValue(itemsDatastring, "c:"));
        form.AddField("sik_d", soru_cek.GetDataValue(itemsDatastring, "d:"));
        form.AddField("cevap", soru_cek.GetDataValue(itemsDatastring, "cevap:"));
        form.AddField("dil", dil.value.ToString());
        form.AddField("zorluk", zorluk.value.ToString());
        WWW www = new WWW(url1, form);
        StartCoroutine(WaitForRequest(www));
    }
    public void sil()
    {
        WWWForm form = new WWWForm();
        form.AddField("durum", "sil");
        form.AddField("id", soru_cek.GetDataValue(itemsDatastring, "sn:"));
        WWW www = new WWW(url1, form);
        StartCoroutine(WaitForRequest(www));
    }
    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            LoadingEkrani.LevelYukle("onay");
        }
        else
        {
            Debug.Log("WWW Eror:" + www.error);
        }
    }

}
