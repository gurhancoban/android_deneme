package com.gurhancoban.list.list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by omer on 2.05.2017.
 */

public class MainScreen extends Activity{

    private FirebaseAuth mAuth;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        mAuth=FirebaseAuth.getInstance();

        Button cikis=(Button)findViewById(R.id.cikis);
        cikis.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                    FirebaseAuth.getInstance().signOut();
                    Database db = new Database(getApplicationContext());
                    db.resetTables();
                Intent ana=new Intent(MainScreen.this,MainActivity.class);
                MainScreen.this.startActivity(ana);
                MainScreen.this.finish();
            }
        });
    }
}
