package com.gurhancoban.list.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


/**
 * Created by omer on 1.05.2017.
 */

public class Login extends BaseActivity implements
        View.OnClickListener {
    private static final String TAG="EmailPassword";

    private FirebaseAuth mAuth;
    private EditText mEmailField;
    private EditText mPasswordField;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        Button kayit=(Button)findViewById(R.id.button3);
        kayit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent kayit_ekran=new Intent(Login.this,Signup.class);
                Login.this.startActivity(kayit_ekran);
                Login.this.finish();
            }
        });

        mEmailField =(EditText) findViewById(R.id.editText8);
        mPasswordField=(EditText) findViewById(R.id.editText9);

        findViewById(R.id.button).setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
    }

    private void signIn(final String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            //
                            Database db = new Database(getApplicationContext());
                            db.resetTables();
                            db.kullaniciEkle(email,"","");
                            Intent anaekran=new Intent(Login.this, MainScreen.class);
                            Login.this.startActivity(anaekran);
                            Login.this.finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]

                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }
    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
    }
    @Override
    public void onClick(View v){
        int i=v.getId();
        if(i==R.id.button){
            signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
        }
    }
}

