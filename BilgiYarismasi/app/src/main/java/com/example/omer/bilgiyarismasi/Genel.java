package com.example.omer.bilgiyarismasi;

/**
 * Created by omer on 2.04.2017.
 */

import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

public class Genel {
    public static boolean giriskontrol(Context context){
        Database db = new Database(context);
        int count = db.getRowCount();// databasedeki table logindeki row sayısı
        if(count > 0){//0 dan fazla ise giriş yapmıs önceden demek
            //kullanıcı giriş yapmıs
            return true;
        }
        return false;
    }
}
