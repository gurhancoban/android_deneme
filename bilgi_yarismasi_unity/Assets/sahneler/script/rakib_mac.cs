﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class rakib_mac : MonoBehaviour {
    public string url1 = "http://www.gurhancoban.com/android/bilgi/soru_cek.php?durum=rakib_mac";
    public string url2 = "http://gurhancoban.com/android/bilgi/soru_cek.php?durum=rakib_soru";
    public string url3 = "http://gurhancoban.com/android/bilgi/veri.php";
    public string url_rakib = "http://gurhancoban.com/android/bilgi/rakib.php";
    public static string itemsDatastring1;
    public string itemsDatastring;
    public string cevap;
    public int sorulma;
    public int bilinme;
    public int oran;
    public int id;
    public Button a;
    public Button b;
    public Button c;
    public Button d;
    public Text soru;
    public string durum = "bos";
    public string dogru;
    public float zaman = 30.0f;
    public Text text;
    public Text uyari;
    public bool zamanlayici = false;
    public static int soru_no = 0;
    public static int[] m_soru_sn;
    private static string cevaplar;
    public WWW itemsdata;
    public WWW rakibim;
    private static string gameid;
    public string cevabim;
    public static int dogru_sayisi = 0;
    // Use this for initialization
    void Start () {
        if (soru_no == 0)
        {
            
            StartCoroutine(sorular());
            StopCoroutine(sorular());
            m_soru_sn = new int[10];
            m_soru_sn[0] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id1:"));
            m_soru_sn[1] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id2:"));
            m_soru_sn[2] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id3:"));
            m_soru_sn[3] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id4:"));
            m_soru_sn[4] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id5:"));
            m_soru_sn[5] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id6:"));
            m_soru_sn[6] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id7:"));
            m_soru_sn[7] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id8:"));
            m_soru_sn[8] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id9:"));
            m_soru_sn[9] = int.Parse(soru_cek.GetDataValue(PlayerPrefs.GetString("sorular"), "id10:"));
            StartCoroutine(sorual(m_soru_sn[soru_no]));
            StopCoroutine(sorual(m_soru_sn[soru_no]));
            zamanlayici = true;
        }
        else if (soru_no > 9)
        {
            soru_no = 0;
            cevaplari_gonder();
            uyari.text = "Sorular Bitmiştir. Rakibiniz/Sonuç Bekleniyor.";
            bekleme(1);
            PlayerPrefs.DeleteKey("rakib");
            PlayerPrefs.DeleteKey("sorular");
            cevaplar = "";
            LoadingEkrani.LevelYukle("oyunlarim");//sonuc_sayfasiniyap
        }
        else
        {
            StartCoroutine(sorual(m_soru_sn[soru_no]));
            StopCoroutine(sorual(m_soru_sn[soru_no]));
            zamanlayici = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (zamanlayici == true)
        {
            zaman -= Time.deltaTime;
            text.text = zaman.ToString("0");
            if (zaman <= 0)
            {
                zamanlayici = false;
                uyari.text = "Süre Doldu";
                //bekleme(1);
                //LoadingEkrani.LevelYukle("anasayfa");
                yanlis_arttirma("-");

            }
        }
    }
    IEnumerator sorular()
    {
        Debug.Log(oyunlarim.gameid);
        itemsdata = new WWW(url1 + "&gameid=" + oyunlarim.gameid);
        yield return itemsdata;
        PlayerPrefs.SetString("sorular", itemsdata.text);
        
    }
    IEnumerator sorual(int id1)
    {
        //Debug.Log("id:"+id1);
        WWW itemsdata2 = new WWW(url2 + "&id=" + id1);
        yield return itemsdata2;
        itemsDatastring = itemsdata2.text;
        //Debug.Log(itemsDatastring);
        soru.text = soru_cek.GetDataValue(itemsDatastring, "soru:");
        a.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring, "a:");
        b.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring, "b:");
        c.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring, "c:");
        d.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring, "d:");
        cevap = soru_cek.GetDataValue(itemsDatastring, "cevap:");
        sorulma = int.Parse(soru_cek.GetDataValue(itemsDatastring, "sorulma:"));
        bilinme = int.Parse(soru_cek.GetDataValue(itemsDatastring, "bilinme:"));
        id = int.Parse(soru_cek.GetDataValue(itemsDatastring, "sn:"));
        soru_no++;
    }
    public void cevapla()
    {
        cevabim = EventSystem.current.currentSelectedGameObject.name.ToUpper();
        Debug.Log(cevabim);
        if (cevap == EventSystem.current.currentSelectedGameObject.name.ToUpper())
        {
            var colors = EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image;
            colors.color = new Color32(19, 98, 55, 255);
            EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image = colors;
            durum = "doğru";
            dogru_arttirma();
            cevaplar = cevaplar + "c" + soru_no + ":" + cevabim + "||";
        }
        else
        {
            var colors = EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image;
            colors.color = new Color32(159, 28, 28, 255);
            EventSystem.current.currentSelectedGameObject.GetComponent<Button>().image = colors;
            durum = "yanlış";
            yanlis_arttirma(cevabim);
        }
    }
    public void dogru_arttirma()
    {
        dogru_sayisi++;
        a.enabled = false;
        b.enabled = false;
        c.enabled = false;
        d.enabled = false;
        kullanici.ds++;
        bilinme++;
        sorulma++;
        WWWForm form = new WWWForm();
        form.AddField("durum", "guncelle");
        form.AddField("id", id);
        form.AddField("sorulma", sorulma);
        form.AddField("bilinme", bilinme);
        //Debug.Log(sorulma + "," + bilinme);
        //LoadingEkrani.LevelYukle(SceneManager.GetActiveScene().name);
        WWW www = new WWW(url3, form);

        StartCoroutine(WaitForRequest(www, "dogru"));
    }
    public void yanlis_arttirma(string durum)
    {

        cevaplar = cevaplar + "c" + soru_no + ":" + durum + "||";
        kullanici.ys++;
        a.enabled = false;
        b.enabled = false;
        c.enabled = false;
        d.enabled = false;
        sorulma++;
        WWWForm form = new WWWForm();
        form.AddField("durum", "guncelle");
        form.AddField("id", id);
        form.AddField("sorulma", sorulma);
        form.AddField("bilinme", bilinme);
        WWW www = new WWW(url3, form);

        StartCoroutine(WaitForRequest(www, "yanlis"));
    }

    public void cevaplari_gonder()
    {
        WWWForm form = new WWWForm();
        form.AddField("durum", "mac_cevap");
        form.AddField("cevap", cevaplar);
        form.AddField("gameid", oyunlarim.gameid);
        form.AddField("kazanilan", oyunlarim.davet_kazanilans);
        form.AddField("kaybedilen", oyunlarim.davet_kaybedilens);
        form.AddField("sender_email", oyunlarim.gonderen_email);
        form.AddField("dogru_sayisi", dogru_sayisi);
        WWW www = new WWW(url3, form);
        //Debug.Log("gameid:" + gameid + "cevaplar:" + cevaplar + "dogruSayisi:" + dogru_sayisi);
        StartCoroutine(cevap_kaydi(www));
    }

    IEnumerator WaitForRequest(WWW www, string sonuc)
    {
        yield return www;
        if (www.error == null)
        {
            if (sonuc == "dogru")
            {
                LoadingEkrani.LevelYukle(SceneManager.GetActiveScene().name);
            }
            if (sonuc == "yanlis")
            {
                LoadingEkrani.LevelYukle(SceneManager.GetActiveScene().name);
            }

        }
        else
        {
            Debug.Log("WWW Eror:" + www.error);
        }
    }
    IEnumerator cevap_kaydi(WWW www)
    {
        yield return www;
        if (www.error == null)
        {

            Debug.Log(www.responseHeaders);
        }
        else
        {
            Debug.Log("WWW Eror:" + www.error);
        }
    }
    IEnumerator bekleme(int sure)
    {
        yield return new WaitForSeconds(sure);
    }
}
