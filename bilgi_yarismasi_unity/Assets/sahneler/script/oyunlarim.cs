﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class oyunlarim : MonoBehaviour {
    public string url1 = "http://gurhancoban.com/android/bilgi/oyunlarim.php";
    public string itemsDatastring="";
    public string itemsDatastring2="";
    public Text bekleyen_o1;
    public Text kazandigi_o1;
    public Text kaybettigi_o1;
    public Image bekleyenresim_o1;
    public Text davet_o;
    public Text davet_kazanilan;
    public Text davet_kaybedilen;
    public Image davet_resim;
    public static string gameid="";
    public int davet_sira = 1;
    public int davet=0;
    public Text davet_deger;
    public static string gonderen_email="";
    public static int davet_kazanilans=0;
    public static int davet_kaybedilens=0;
    //public Button bekleyen_next;
    //public Button bekleyen_before;
    
    public static int bekleyen_sira = 1;
    public int bekleyen=0;
    public Text bekleyen_deger;

    public Button kabul;
    public Button red;
    public CanvasGroup bekleyen1;

    
    // Use this for initialization
    void Start () {
        StartCoroutine(durum_cek());
        StopCoroutine(durum_cek());
        StartCoroutine(davet_cek());
        StopCoroutine(davet_cek());
        
    }
	
	// Update is called once per frame
	void Update () {
       
        
    }
    IEnumerator davet_cek()
    {
        WWW itemsdata2 = new WWW(url1 + "?durum=davet&user_name=" + PlayerPrefs.GetString("email"));
        yield return itemsdata2;
        itemsDatastring2 = itemsdata2.text;
        davet = int.Parse(soru_cek.GetDataValue(itemsDatastring2, "davet:"));
        if (davet > 0)
        {
            if (davet > 5)
            {
                davet_deger.text = "5+";
                davet = 5;
            }
            else
            {
                davet_deger.text = davet.ToString();
            }
            gonderen_email = soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira + "gonderen:");
            gameid = soru_cek.GetDataValue(itemsDatastring2, "gameid" + davet_sira + ":");
            davet_o.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira + ":");
            davet_kazanilans = int.Parse(soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira + "kazanilan:"));
            davet_kaybedilens = int.Parse(soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira + "kaybedilen:"));
            davet_kazanilan.GetComponentInChildren<Text>().text = "Kazandğı Ouyun:" + davet_kazanilans;
            davet_kaybedilen.GetComponentInChildren<Text>().text = "Kaybettiği Oyun: " + davet_kaybedilens;
        }
        else
        {
            davet_deger.text = "0";
            davet_o.GetComponentInChildren<Text>().text = "--";
            davet_kazanilan.GetComponentInChildren<Text>().text = "Kazandğı Ouyun: --";
            davet_kaybedilen.GetComponentInChildren<Text>().text = "Kaybettiği Oyun: --";
            kabul.enabled = false;
            red.enabled = false;
        }
    }

    IEnumerator durum_cek()
    {
        WWW itemsdata = new WWW(url1+"?durum=bekleyen&user_name="+PlayerPrefs.GetString("email"));
        yield return itemsdata;
        itemsDatastring = itemsdata.text;
        bekleyen = int.Parse(soru_cek.GetDataValue(itemsDatastring, "bekleyen:"));
        if (bekleyen > 5)
        {
            bekleyen_deger.text = "5+";
            bekleyen = 5;
        }else
        {
            bekleyen_deger.text = bekleyen.ToString();
        }
        
        //Debug.Log(bekleyen_sira);
        bekleyen_o1.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + ":"); 
        kazandigi_o1.GetComponentInChildren<Text>().text= "Kazandıği Oyun: "+soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + "kazanilan:");
        kaybettigi_o1.GetComponentInChildren<Text>().text="Kaybettiği Oyun: " + soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + "kaybedilen:");
    }
    public void next()
    {
        Debug.Log(bekleyen_sira);
        if (bekleyen > bekleyen_sira) {
            bekleyen_sira++;
            bekleyen_o1.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + ":");
            kazandigi_o1.GetComponentInChildren<Text>().text = "Kazandiği Oyun: " + soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + "kazanilan:");
            kaybettigi_o1.GetComponentInChildren<Text>().text = "Kaybettiği Oyun: " + soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + "kaybedilen:");
        }
    }
    public void before()
    {
        if (bekleyen_sira>1)
        {
            bekleyen_sira =bekleyen_sira-1;
            bekleyen_o1.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + ":");
            kazandigi_o1.GetComponentInChildren<Text>().text = "Kazandiği Oyun: " + soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + "kazanilan:");
            kaybettigi_o1.GetComponentInChildren<Text>().text = "Kaybettiği Oyun: " + soru_cek.GetDataValue(itemsDatastring, "reciver" + bekleyen_sira + "kaybedilen:");
        }
    }
    public void next_davet()
    {
        
        if (davet > davet_sira)
        {
            
            davet_sira++;
            gameid = soru_cek.GetDataValue(itemsDatastring2, "gameid" + davet_sira + ":");
            davet_o.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira + ":");
            davet_kazanilan.GetComponentInChildren<Text>().text = "Kazandiği Oyun: " + soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira+ "kazanilan:");
            davet_kaybedilen.GetComponentInChildren<Text>().text = "Kaybettiği Oyun: " + soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira+ "kaybedilen:");
        }
    }
    public void before_davet()
    {
        if (davet_sira > 1)
        {
            davet_sira = davet_sira- 1;
            gameid = soru_cek.GetDataValue(itemsDatastring2, "gameid" + davet_sira + ":");
            davet_o.GetComponentInChildren<Text>().text = soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira+ ":");
            davet_kazanilan.GetComponentInChildren<Text>().text = "Kazandiği Oyun: " + soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira+ "kazanilan:");
            davet_kaybedilen.GetComponentInChildren<Text>().text = "Kaybettiği Oyun: " + soru_cek.GetDataValue(itemsDatastring2, "sender" + davet_sira+ "kaybedilen:");
        }
    }

    public void oyun_kabul()
    {
        LoadingEkrani.LevelYukle("rakib_mac");
    }

    public void oyun_red()
    {
        WWWForm form = new WWWForm();
        form.AddField("sonuc", "c");
        form.AddField("gameid", gameid);
        WWW www = new WWW(url1 + "?durum=red", form);

        StartCoroutine(oyun_red_request(www));
    }
    IEnumerator oyun_red_request(WWW www)
    {
        yield return www;
        if (www.error==null)
        {
            Debug.Log(davet_sira + ", " + davet);
            if (davet_sira < davet)
            {
                Debug.Log("dogru");
                next_davet();
            }
            else
            {
                LoadingEkrani.LevelYukle("oyunlarim");
            }
        }
        else
        {
            
        }
    }
}
