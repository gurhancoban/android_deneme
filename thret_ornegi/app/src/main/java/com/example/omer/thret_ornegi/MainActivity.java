package com.example.omer.thret_ornegi;

import android.content.Intent;
//import android.support.v4.widget.SearchViewCompatIcs;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Thread Myscreen = new Thread() {
            public void run() {
                try {
                    sleep(5000);
                    startActivity(new Intent(getApplicationContext(), Javasi.class));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        Myscreen.start();
    }
}