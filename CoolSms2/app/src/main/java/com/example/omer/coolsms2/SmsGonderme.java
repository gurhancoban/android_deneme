package com.example.omer.coolsms2;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;
import android.view.KeyEvent;




/**
 * Created by omer on 4.04.2017.
 */

public class SmsGonderme extends Activity{
    private  static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0;
    Button sndBtn;
    EditText txtPhone;
    EditText txtMessage;
    String phoneNo;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_gonderme);

        sndBtn =(Button)findViewById(R.id.button);
        txtPhone=(EditText)findViewById(R.id.editText1);
        txtMessage=(EditText)findViewById(R.id.editText2);

        sndBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
               // sendSMSMessage();
                phoneNo=txtPhone.getText().toString();
                message=txtMessage.getText().toString();
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, message, null, null);
                Toast.makeText(getApplicationContext(), "SMS Gönderildi.",
                        Toast.LENGTH_LONG).show();
                txtMessage.setText("");
            }

        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent=new Intent(SmsGonderme.this,MainActivity.class);
            SmsGonderme.this.startActivity(intent);
            SmsGonderme.this.finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    protected void sendSMSMessage() {
        phoneNo = txtPhone.getText().toString();
        message = txtMessage.getText().toString();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNo, null, message, null, null);
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

    }
}