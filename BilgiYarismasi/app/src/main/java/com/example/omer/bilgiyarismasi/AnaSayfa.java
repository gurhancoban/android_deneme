package com.example.omer.bilgiyarismasi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by omer on 2.04.2017.
 */

public class AnaSayfa extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ana_sayfa);
        final ImageButton yenioyun=(ImageButton) findViewById(R.id.yenioyun);
        yenioyun.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent=new Intent(AnaSayfa.this, Yeni_Oyun.class);
                AnaSayfa.this.startActivity(intent);
                AnaSayfa.this.finish();
            }
        });
    }
}
