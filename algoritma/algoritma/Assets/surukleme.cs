﻿using UnityEngine;
using System.Collections;

public class surukleme : MonoBehaviour {

    public GameObject gameObjectToDrag;
    public Vector3 GOcenter;
    public Vector3 touchPosition;
    public Vector3 offset;
    public Vector3 newGOcenter;

    RaycastHit hit;
    public bool draggingmode = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                gameObjectToDrag = hit.collider.gameObject;
                GOcenter = gameObjectToDrag.transform.position;
                touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                offset = touchPosition - GOcenter;
                draggingmode = true;
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (draggingmode)
            {
                touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                newGOcenter = touchPosition - offset;
                newGOcenter.z = -8;
                gameObjectToDrag.transform.position = new Vector3(newGOcenter.x, newGOcenter.y, newGOcenter.z);
                
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            newGOcenter.z = 0;
            gameObjectToDrag.transform.position = new Vector3(newGOcenter.x, newGOcenter.y, newGOcenter.z);
            //Debug.Log(newGOcenter);
            draggingmode = false;
        }
        

	}
}
