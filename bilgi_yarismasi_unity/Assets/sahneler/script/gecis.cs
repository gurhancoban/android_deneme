﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class gecis : MonoBehaviour {
    public string[] sahneler;
    public string url = "http://www.gurhancoban.com/android/bilgi/veri.php";

	// Use this for initialization
	void Start () {
        sahneler = new string[10];
        sahneler[0] = "login";
        sahneler[1] = "anasayfa";
        sahneler[2] = "yeni_oyun";
        sahneler[3] = "hazirlik";
        sahneler[4] = "rakib";
        sahneler[5] = "soru_giris";
        sahneler[6] = "onay";
        sahneler[7] = "ayarlar";
        sahneler[8] = "oyunlarim";
        sahneler[9] = "rakib_mac";
    }
	
	// Update is called once per frame
	void Update () {
        int deger = SceneManager.GetActiveScene().buildIndex;
        //Debug.Log(deger);
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (deger == 2)
            {
                LoadingEkrani.LevelYukle(sahneler[1]);
            }
            else if(deger==3)
            {
                kullanici_bilgi_kayit();
                LoadingEkrani.LevelYukle("anasayfa");
            }
            else if (deger == 4)
            {
                kullanici_bilgi_kayit();
                LoadingEkrani.LevelYukle("anasayfa");
            }
            else if ((deger == 5) || (deger == 6) || (deger == 8) || (deger == 8) || (deger == 8))
            {
                LoadingEkrani.LevelYukle("anasayfa");
            }
            else
            {
                PlayerPrefs.SetString("isim", kullanici.isim);
                PlayerPrefs.SetInt("ds", kullanici.ds);
                PlayerPrefs.SetInt("ys", kullanici.ys);
                PlayerPrefs.SetInt("kazanilan", kullanici.kazanilan);
                PlayerPrefs.SetInt("kaybedilen", kullanici.kaybedilen);

                WWWForm form = new WWWForm();
                form.AddField("durum", "kullanici_durum");
                form.AddField("ds", kullanici.ds);
                form.AddField("ys", kullanici.ys);
                form.AddField("kazanilan", kullanici.kazanilan);
                form.AddField("kaybedilen", kullanici.kaybedilen);
                form.AddField("email", PlayerPrefs.GetString("email").ToString());
                WWW www = new WWW(url, form);
                StartCoroutine(WaitForRequest(www));               
            }
        }
    }
    public void yeni_oyun()
    {
        //SceneManager.LoadScene(1);
        LoadingEkrani.LevelYukle(sahneler[2]);
    }
    public void hazirlik()
    {
        //SceneManager.LoadScene(1);
        //Debug.Log(sahneler[2]);
        LoadingEkrani.LevelYukle(sahneler[3]);
    }
    public void rakib()
    {
        //SceneManager.LoadScene(1);
        //Debug.Log(sahneler[3]);
        LoadingEkrani.LevelYukle(sahneler[4]);
    }
    public void soru_giris()
    {
        LoadingEkrani.LevelYukle(sahneler[5]);
    }
    public void anasayfa()
    {
        LoadingEkrani.LevelYukle("anasayfa");
    }
    public void onay()
    {
        //tiklanan butonun ismini al
        LoadingEkrani.LevelYukle("onay");
    }
    public void cikis()
    {
        PlayerPrefs.SetString("isim", kullanici.isim);
        PlayerPrefs.SetInt("ds", kullanici.ds);
        PlayerPrefs.SetInt("ys", kullanici.ys);
        PlayerPrefs.SetInt("kazanilan", kullanici.kazanilan);
        PlayerPrefs.SetInt("kaybedilen", kullanici.kaybedilen);

        WWWForm form = new WWWForm();
        form.AddField("durum", "kullanici_durum");
        form.AddField("ds", kullanici.ds);
        form.AddField("ys", kullanici.ys);
        form.AddField("kazanilan", kullanici.kazanilan);
        form.AddField("kaybedilen", kullanici.kaybedilen);
        form.AddField("email", PlayerPrefs.GetString("email").ToString());
        WWW www = new WWW(url, form);
        StartCoroutine(WaitForRequest(www));
        PlayerPrefs.DeleteAll();
        Application.Quit();
    }
    public void ayarlar()
    {
        LoadingEkrani.LevelYukle("ayarlar");
    }

    public void oyunlarim()
    {
        LoadingEkrani.LevelYukle("oyunlarim");
    }
    public void kullanici_bilgi_kayit()
    {
        PlayerPrefs.SetInt("ds", kullanici.ds);
        PlayerPrefs.SetInt("ys", kullanici.ys);
        WWWForm form = new WWWForm();
        form.AddField("durum", "kullanici_bilgi_h");
        form.AddField("dogru_sayisi", kullanici.ds);
        form.AddField("yanlis_sayisi", kullanici.ys);
        form.AddField("email", PlayerPrefs.GetString("email"));

        WWW www = new WWW(url, form);
        StartCoroutine(kullanici_kaydi_bekle(www));
    }
    IEnumerator kullanici_kaydi_bekle(WWW www)
    {
        yield return www;
    }
    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            Application.Quit();
        }
        else
        {
            Debug.Log("WWW Eror:" + www.error);
        }
    }
}
