package com.gurhancoban.list.list;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by omer on 1.05.2017.
 */

public class Signup extends BaseActivity implements
    View.OnClickListener{
        private static final String TAG="EmailPassword";

        private EditText mEmailField;
        private EditText mPasswordField;

        private FirebaseAuth mAuth;

        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.signup);

            mEmailField=(EditText) findViewById(R.id.editText);
            mPasswordField=(EditText) findViewById(R.id.editText2);

            findViewById(R.id.button2).setOnClickListener(this);

            mAuth=FirebaseAuth.getInstance();
        }

        @Override
        public void onStart(){
            super.onStart();
            FirebaseUser currentUser=mAuth.getCurrentUser();
            updateUI(currentUser);
        }
        private void createAccount(String email, String password){
        Log.d(TAG, "createAccounf:"+email);
        if(!validateForm()){
            return;
        }
        showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "createUserWithEmaşl:Success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        }else{
                            Log.w(TAG, "currentUserWithEmail:failer",task.getException());
                            Toast.makeText(Signup.this,"Authentication Failed.", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        hideProgressDialog();
                    }
                });
    }
    private boolean validateForm(){
        boolean valid=true;
        String email=mEmailField.getText().toString();
        if(TextUtils.isEmpty(email)){
            mEmailField.setError("Required.");
            valid=false;
        }else{
            mEmailField.setError(null);
        }
        return valid;
    }
    private void updateUI(FirebaseUser user){
        hideProgressDialog();

    }

    @Override
    public void onClick(View v){
        int i=v.getId();
        if(i==R.id.button2){
            createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
        }
    }
}

