﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingEkrani : MonoBehaviour
{
    // LoadingEkrani scriptine kolayca erisebilmek icin static degisken
    private static LoadingEkrani instance = null;

    public Image panel;
    public Text yazi;

    // Degeri arttikca yumusak gecis animasyonu hizlanir
    public float animasyonHizi = 1.0f;

    // Level yukleme islemi
    private AsyncOperation loadingIslemi;

    // Yeni bir level yukle
    public static void LevelYukle(string levelAdi)
    {
        // Eger sahnede bir LoadingEkrani yoksa yeni bir tane olustur
        if (instance == null)
        {
            instance = Instantiate(Resources.Load<GameObject>("LoadingEkrani")).GetComponent<LoadingEkrani>();
            DontDestroyOnLoad(instance.gameObject); // Loading ekrani sahneler arasi geciste kaybolmasin
        }
        // Loading ekranini aktiflestir
        instance.gameObject.SetActive(true);
        // Yeni leveli yuklemeye basla
        instance.loadingIslemi = SceneManager.LoadSceneAsync(levelAdi);
        // Yeni levelin yuklenmesi tamamlansa bile hemen yeni levela gecis yapma
        instance.loadingIslemi.allowSceneActivation = false;
    }

    void Awake()
    {
        // En basta loading ekranini gorunmez yap
        Color c = panel.color;
        c.a = 0f;
        panel.color = c;

        c = yazi.color;
        c.a = 0f;
        yazi.color = c;
    }

    void Update()
    {
        // Yukleme yuzdesini guncelle
        //yazi.text = "Yukleniyor %" + (loadingIslemi.progress * 100f);

        // Yukleme tamamlandiysa
        if (loadingIslemi.isDone)
        {
            // Loading ekranini yumusak bir sekilde kaybet (fade out)
            Color c = panel.color;
            c.a -= animasyonHizi * Time.deltaTime;
            panel.color = c;

            c = yazi.color;
            c.a -= animasyonHizi * Time.deltaTime;
            yazi.color = c;

            // Loading ekrani tamamen kaybolduysa objeyi deaktif et
            if (c.a <= 0)
            {
                gameObject.SetActive(false);
            }
        }
        else // Yukleme islemi bitmediyse
        {
            // Loading ekranini yumusak bir sekilde gorunur yap (fade in)
            Color c = panel.color;
            c.a += animasyonHizi * Time.deltaTime;
            panel.color = c;

            c = yazi.color;
            c.a += animasyonHizi * Time.deltaTime;
            yazi.color = c;

            // Loading ekrani tamamen gorunur hale geldiyse
            if (c.a >= 1)
            {
                // Artik yukleme islemi bitince yeni levela gecebilelim
                loadingIslemi.allowSceneActivation = true;
            }
        }
    }
}