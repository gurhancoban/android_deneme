package com.example.omer.bilgiyarismasi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class Giris extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giris);

        if (InternetKontrol()) {
            Thread MyScreen = new Thread(){
                public void run() {
                        try {
                            sleep(2000);
                            Database db1 = new Database(getApplicationContext());
                            int count = db1.getRowCount();// databasedeki table logindeki row sayısı
                            if(count > 0){
                                sleep(5000);
                                Intent anasayfa=new Intent(getApplicationContext(),AnaSayfa.class );
                                startActivity(anasayfa);
                            }else{
                                Intent login_page = new Intent(getApplicationContext(),LoginPage.class );
                                startActivity(login_page);
                                //finish();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            finish();
                        }

                }
               };
            MyScreen.start();
        }
        else {
            try{
                AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(this); //Mesaj Penceresini Yaratalım
                alertDialogBuilder.setTitle("Dikkat!").setCancelable(false).setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) { //Eğer evet butonuna basılırsa
                        dialog.dismiss();
                        android.os.Process.killProcess(android.os.Process.myPid());
//Uygulamamızı sonlandırıyoruz.
                    }
                }).setMessage("İnternet Bağantınız aktif değil Lütfen Bağlantıyı aktif ettikten sonra tekrar deneyiniz.");
                alertDialogBuilder.create().show();
//son olarak alertDialogBuilder'ı oluşturup ekranda görüntületiyoruz.
               // return super.onKeyDown(keyCode, event);
            }
            catch(IllegalStateException e){ //yapımızı try-catch blogu içerisine aldık
//hata ihtimaline karşı.
                e.printStackTrace();
            }

        }
    }

    public boolean InternetKontrol(){
        Genel  genel=new Genel();
        ConnectivityManager maneger=(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(maneger.getActiveNetworkInfo()!=null &&maneger.getActiveNetworkInfo().isAvailable() && maneger.getActiveNetworkInfo().isConnected()){
            return true;
        }
        else{
            return false;
        }
    }

    public static boolean giriskontrol(Context context){

        Database db1 = new Database(context);
        int count = db1.getRowCount();// databasedeki table logindeki row sayısı
        if(count > 0){//0 dan fazla ise giriş yapmıs önceden demek
            //kullanıcı giriş yapmıs
            return true;
        }
        return false;
    }
}
